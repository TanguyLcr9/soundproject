﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tempete : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip sansTonnerre;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.clip = sansTonnerre;
            audioSource.loop = true;
        }
    }
}
