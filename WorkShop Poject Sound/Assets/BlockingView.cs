﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockingView : MonoBehaviour
{
    public GameObject View;

    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            View.SetActive(false);
        }
        else 
        {
            View.SetActive(true);
        }
    
    }
}
