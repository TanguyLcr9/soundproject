﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Amant : MonoBehaviour
{
    public Transform target;
    public AudioClip breakSound;
    public AudioSource audioSource, footSteps;
    public Player player;
  

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Flee()
    {
        transform.DOMove(target.position,2f).SetEase(Ease.OutQuint);
        StartCoroutine(Run());
        Debug.Log("Breaking");
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Breaking");
        if (other.CompareTag("Window"))
        {

        }

    }

    IEnumerator Run()
    {
        Debug.Log("Breaking");
        for (int i = 0; i< 10; i++)
        {
            footSteps.clip = player.footSteps[Random.Range(0, 11)];
            footSteps.Play();
            yield return new WaitForSeconds(0.12f);
        }
        audioSource.clip = breakSound;
        audioSource.Play();


    }

}
