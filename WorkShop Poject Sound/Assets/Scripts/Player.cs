﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using DG.Tweening;

public class Player : MonoBehaviour
{
    [Range(0, 5)]
    public float speed=1;
    public GameObject door;
    public AudioSource foots, speaker, ail;
    public List<AudioClip> footSteps;
    public List<AudioClip> speaking;
    public AudioClip doorClac;
    public string floorType;
    public AudioMixer mixer;
    public AudioMixerSnapshot[] snapshots;
    public List<AudioClip> rage;
    private bool havePlay = false;



    private bool isHitingWall;
    private float horizontal;
    private float vertical;
    private float timer;
    bool isEntered;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FootsSteps());
        floorType = "";
        isEntered = false;
        transform.DOMove(door.transform.position + Vector3.forward * 1.5f, 10f).SetEase(Ease.InQuint);
    }

    // Update is called once per frame
    void Update()
    {

        if (isEntered)
        {
            if (!isHitingWall)// Si le joueur n'a pas taper dans un mur
            {
                transform.position += new Vector3(horizontal, 0, vertical).normalized * Time.deltaTime * speed;
                horizontal = Input.GetAxis("Horizontal");
                vertical = Input.GetAxis("Vertical");
            }
            else // sinon il retourne en arrière le temps d'une seconde
            {
                timer += Time.deltaTime;
                if (timer <= 0.5)
                {
                    transform.position -= new Vector3(horizontal, 0, vertical).normalized * Time.deltaTime * speed;
                    
                }
                else if (timer > 0.6)// après 0.6 secondes le joueur reprend le controle
                {
                    isHitingWall = false;
                    
                    timer = 0;
                }
            }
        }
        else
        {

                
            if (Vector3.Distance(door.transform.position + Vector3.forward * 1.5f, transform.position) < 0.1f)
            {
                StartCoroutine(door.GetComponent<door>().DoorClosingSound());
                isEntered = true;
                door.GetComponent<Collider>().enabled = true;
            }
        }

    }

    IEnumerator FootsSteps()
    {
            foots.clip = footSteps[23];
            foots.Play();
            yield return new WaitForSeconds(10f);
        foots.Stop();

        while (true)
        {
            yield return new WaitUntil(() => isEntered);
            yield return new WaitUntil(() => Input.anyKey);
            switch (floorType)
            {
                case "Wood":
                    foots.clip = footSteps[Random.Range(0, 11)];
                    break;
                case "Solid":
                    foots.clip = footSteps[Random.Range(12, 17)];
                    break;
                case "Stare":
                    foots.clip = footSteps[Random.Range(18, 22)];
                    break;
            }
                foots.Play();

            yield return new WaitForSeconds(0.8f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Wall"))
        {
            isHitingWall = true;
            ail.clip = rage[Random.Range(0, rage.Count)];
            ail.Play();
            return;
        }

        if (other.gameObject.CompareTag("Stare Floor"))
        {
            floorType = "Stare";
            return;
        }

        if (other.gameObject.CompareTag("Solid Floor"))
        {
            floorType = "Solid";
            if (!isEntered)
            {
                float[] weights = { 1, 0 };
                mixer.TransitionToSnapshots(snapshots, weights, 1f);
            }
            return;
        }

        if (other.gameObject.CompareTag("Wood Floor"))
        {
            floorType = "Wood";
            return;
        }
    }
}
