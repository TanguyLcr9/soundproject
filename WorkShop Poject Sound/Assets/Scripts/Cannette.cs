﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannette : MonoBehaviour
{
    AudioSource source;
    public List<AudioClip> cannettesSounds;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(PlaySound(Random.Range(0, cannettesSounds.Count)));
    }

    IEnumerator PlaySound(int ran)
    {

        source.clip = cannettesSounds[ran];
        source.Play();
        yield return new WaitForSeconds(3f);
        source.Stop();
    }
}
