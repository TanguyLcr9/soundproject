﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

using DG.Tweening;

public class Coit : MonoBehaviour
{
    public List<Transform> targets;
    public int positionIndex =0;
    public AudioClip[] scream;
    public AudioSource[] audioSource;
    public Amant amant;
    public Girl girl;
    public Transform amantTarget;
    public AudioMixer mixer;
    public AudioMixerSnapshot[] snapshots;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (positionIndex != targets.Count)
            {
                transform.DOMove(targets[positionIndex].position, 2f).SetEase(Ease.OutExpo);
                positionIndex++;
                if(positionIndex == targets.Count - 1)
                {
                    float[] weights = { 0, 1,0 };
                    mixer.TransitionToSnapshots(snapshots, weights, 5f);
                }
            }
            else
            {
                
                StartCoroutine(Screaming());
                

            }
        }
    }

    IEnumerator Screaming()
    {
        audioSource[2].Stop();

        audioSource[0].clip = scream[2];
        audioSource[0].Play();
        audioSource[0].loop = false;

        yield return new WaitForSeconds(0.4f);
        audioSource[1].clip = scream[1];
        audioSource[1].Play();
        audioSource[1].loop = false;

        yield return new WaitForSeconds(0.4f);
        audioSource[0].clip = scream[0];
        audioSource[0].Play();

        yield return new WaitForSeconds(1f);

        girl.Flee();
        girl.GetComponent<Collider>().enabled = true;
        girl.audioSource.Play();
        amant.Flee();

        float[] weights = { 0, 0, 1 };
        mixer.TransitionToSnapshots(snapshots, weights, 5f);

       

    }

}
