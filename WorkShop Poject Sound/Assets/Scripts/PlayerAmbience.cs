﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAmbience : MonoBehaviour
{
    public List<AudioClip> Toux;
    public List<AudioClip> Rot;
    public List<AudioClip> Gloups;
    private float timer;
    public AudioSource Player;
    private int ambiance;

    private void Start()
    {
        timer = Random.Range(5, 20);
    }


    private void Update()
    {
        if (timer <= 0)
        {
            ambiance = Random.Range(1,4);

            if (ambiance == 1)
            {
                Debug.Log("Petite toux");
                Player.clip = Toux[Random.Range(0, Toux.Count)];
                Player.Play();
            }

            if (ambiance == 2)
            {
                Debug.Log("Petite rot");
                Player.clip = Rot[Random.Range(0, Rot.Count)];
                Player.Play();
            }

            if(ambiance == 3)
            {
                Player.clip = Gloups[Random.Range(0, Gloups.Count)];
                Player.Play();
            }

            timer = Random.Range(5, 20);
        }
        timer-= Time.deltaTime;
    }

}

