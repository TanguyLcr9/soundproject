﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class door : MonoBehaviour
{
    public AudioClip doorClosing, doorSlap;
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public IEnumerator DoorClosingSound()
    {
        audioSource.clip = doorClosing;
        audioSource.Play();
        yield return new WaitForSeconds(doorClosing.length);

        audioSource.clip = doorSlap;
        audioSource.Play();

        yield return new WaitForSeconds(doorSlap.length);

        Destroy(audioSource);
        Destroy(this);
    }
}
