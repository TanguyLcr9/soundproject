﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

using DG.Tweening;

public class Girl : MonoBehaviour
{
    public List<Transform> target;
    public int positionIndex = 0;
    public AudioMixer mixer;
    public AudioMixerSnapshot[] snapshots;
    public AudioSource audioSource, footSteps, mariSource, souffleSource, cardioSource;
    public AudioClip surprise, criMari, criFemme, hitting;
    public Player player;


    private void Start()
    {

    }

    public void Flee()
    {
        if (positionIndex == target.Count)
        {
            StartCoroutine(KillHer());
        }
        else
        {
            if (positionIndex >= 0)
            {
                float[] weights = { 0, 1, 0 };
                mixer.TransitionToSnapshots(snapshots, weights, 0.5f);
                
            }
            StartCoroutine(Run());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

                Flee();
            
        }
    }
    


    IEnumerator Run()
    {
        Debug.Log("Breaking");
        audioSource.clip = surprise;
        audioSource.Play();
      
        transform.DOMove(target[positionIndex].position, 2f).SetEase(Ease.OutExpo);
        positionIndex++;
        for (int i = 0; i < 6; i++)
        {
            footSteps.clip = player.footSteps[Random.Range(0, 11)];
            footSteps.Play();
            yield return new WaitForSeconds(0.12f);
        }
    }

    IEnumerator KillHer()
    {
        souffleSource.Stop();
        cardioSource.Stop();
        Debug.Log("You killed Her");
        mariSource.clip = criMari;
        mariSource.Play();
        yield return new WaitForSeconds(0.5f);
        audioSource.clip = criFemme;
        audioSource.Play();
        yield return new WaitForSeconds(5f);
        
        float[] weights = { 0, 0, 1 };
        mixer.TransitionToSnapshots(snapshots, weights, 12f);
        yield return null;


    }

}
