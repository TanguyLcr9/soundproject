﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tonerre : MonoBehaviour
{
    public List<AudioClip> tonerre;
    private float timer;
    public AudioSource Sky;

    private void Start()
    {
        timer = Random.Range(300,600);
    }


    private void Update()
    {
        if (timer <= 0)
        {
            Debug.Log("Que tombe le Tonerre");
            Sky.clip = tonerre[Random.Range(0, 2)];
            Sky.Play();
            timer = Random.Range(300, 600);
        }
        timer--;
    }

}
